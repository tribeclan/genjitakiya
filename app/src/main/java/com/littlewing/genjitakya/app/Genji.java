package com.littlewing.genjitakya.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class Genji extends Activity {
    private static int ROW_COUNT = -1;
    private static int COL_COUNT = -1;
    private Context context;
    private Drawable backImage;
    private int [] [] cards;
    private List<Drawable> images;
    private Card firstCard;
    private Card seconedCard;
    private ButtonListener buttonListener;
    private static Object lock = new Object();
    private TableLayout mainTable;
    private UpdateCardsHandler handler;
    private static final int NEW_MENU_ID = Menu.FIRST;
    private static final int EXIT_MENU_ID = Menu.FIRST+1;
    private int count =0;
    private MediaPlayer mp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        Log.i("width height ", "w: " + width + " h: " + height);

        super.onCreate(savedInstanceState);
        progressDialog();
        handler = new UpdateCardsHandler();

        loadImages("vowel");
        setContentView(R.layout.activity_main);
        Button btnnew = (Button)findViewById(R.id.btnnew);
        btnnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                clickSound();
                singleChoiceDialog();
            }
        });

        backImage = getResources().getDrawable(R.drawable.iconfruit);
        buttonListener = new ButtonListener();
        mainTable = (TableLayout)findViewById(R.id.TableLayout03);
        context = mainTable.getContext();
        newGame(6, 8, "vowel");
    }

    public void progressDialog(){
        final ProgressDialog progressDialog = ProgressDialog.show(Genji.this, "", "Loading...", true);

        new Thread(new Runnable(){

            public void run(){
                try {
                    Thread.sleep(100);
                    progressDialog.dismiss();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void newGame(int c, int r, String type) {
        this.loadImages(type);

        ROW_COUNT = r;
        COL_COUNT = c;
        count =0;

        // TODO check out of index
        cards = new int [COL_COUNT] [ROW_COUNT];

        mainTable.removeView(findViewById(R.id.TableRow01) );
        mainTable.removeView(findViewById(R.id.TableRow02) );

        TableRow tr = ((TableRow)findViewById(R.id.TableRow03));
        tr.removeAllViews();

        mainTable = new TableLayout(context);
        tr.addView(mainTable);

        for (int y = 0; y < ROW_COUNT; y++) {
            mainTable.addView(createRow(y)); // TODO
        }

        firstCard=null;
        loadCards();
    }

    private void loadImages(String type) {
        images = new ArrayList<Drawable>();

        if(type == "hira") {
            images = this.loadHira();
        } else if(type == "kata") {
            images = this.loadKata();
        } else if(type == "pinyin") {
            images = this.loadPinyin();
        } else if(type == "vowel") {
            images = this.loadVowel();
        } else if(type == "vowel2") {
            images = this.loadVowel2();
        }
        else {
            // default load hiragana
            images = this.loadHira();
            return;
        }
    }

    private void loadCards(){
        int size = ROW_COUNT*COL_COUNT;

        Log.i("loadCards()","size=" + size);

        ArrayList<Integer> list = new ArrayList<Integer>();

        for(int i=0;i<size;i++){
            list.add(new Integer(i));
        }
        Random r = new Random();

        for(int i=size-1;i>=0;i--){
            int t=0;

            if(i>0){
                t = r.nextInt(i);
            }

            t=list.remove(t).intValue();      // TODO research this
            cards[i%COL_COUNT][i/COL_COUNT]=t%(size/2);

            Log.i("loadCards()", "card["+(i%COL_COUNT)+
                    "]["+(i/COL_COUNT)+"]=" + cards[i%COL_COUNT][i/COL_COUNT]);
        }

    }

    private TableRow createRow(int y){
        TableRow row = new TableRow(context);
        row.setHorizontalGravity(Gravity.CENTER);

        for (int x = 0; x < COL_COUNT; x++) {
            row.addView(createImageButton(x,y));
        }
        return row;
    }

    private View createImageButton(int x, int y){
        Button button = new Button(context);
        button.setBackgroundDrawable(backImage);
        button.setId(100*x+y); // fix me
        button.setOnClickListener(buttonListener);
        return button;
    }

    class ButtonListener implements View.OnClickListener {

        int doubleTap = 0;        // wrap imag when re-tap
        int currentCard = -1;      // current position of Card select

        @Override
        public void onClick(View v) {
            if(currentCard != v.getId()) {
                currentCard = v.getId();
                doubleTap = 0;
                Log.i("doubleTap: ", " tap in another card n = " + doubleTap + " other  card = " + currentCard);
            } else {     // Tap dung card 2,3 ..n lan
                doubleTap++;
            }
            synchronized (lock) {
                if(firstCard!=null && seconedCard != null){  // ???
                    Log.i("2 card", "card 1 = " + firstCard.x + firstCard.y + " 2nd card = " + seconedCard.x + seconedCard.y);
                    return;
                }
                int id = v.getId();
                int x = id/100;
                int y = id%100;
                turnCard((Button)v,x,y);
            }
            Log.i("doubleTap: ", " tap n = " + doubleTap + " cur card = " + currentCard);
        }

        private void turnCard(Button button,int x, int y) {
            if((doubleTap%2) == 0) {
                // TODO fix out_of boundary x, y. Kha nang do doan loadImage voi loadCard()
                // loadImage() co count(imag) ko dong bo: max 10x6 = 60 img.
                button.setBackgroundDrawable(images.get(cards[x][y]));
            } else {
                button.setBackgroundDrawable(backImage);
                firstCard = null;
                return;
            }
            //playSelectedSound();
            if(firstCard==null){         // Handle tap an open card, or close it.
                firstCard = new Card(button,x,y);
            }
            else{
                if(firstCard.x == x && firstCard.y == y){
                    return;
                }
                Log.i("doubleTap: ", " tap n = " + doubleTap + " cur card = " + currentCard);
                seconedCard = new Card(button,x,y);

                TimerTask tt = new TimerTask() {
                    @Override
                    public void run() { // TODO digging this
                        //try{
                        synchronized (lock) {
                            handler.sendEmptyMessage(0);
                        }
                        //}
                        //catch (Exception e) {
                        //	Log.e("E1", e.getMessage());
                        //}
                    }
                };

                Timer t = new Timer(false);
                t.schedule(tt, 300); // delay when swap icon
            }
        }
    }

    class UpdateCardsHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            synchronized (lock) {
                checkCards();
            }
        }
        @SuppressWarnings("deprecation")
        public void checkCards(){

            if(cards[seconedCard.x][seconedCard.y] == cards[firstCard.x][firstCard.y]){
//                playCorrectSound();
                firstCard.button.setVisibility(View.INVISIBLE);
                seconedCard.button.setVisibility(View.INVISIBLE);

                count ++;
                if(count == (ROW_COUNT*COL_COUNT)/2){
                    winDialog();
                }
            }
            else {
                seconedCard.button.setBackgroundDrawable(backImage );
                firstCard.button.setBackgroundDrawable(backImage);
            }

            firstCard=null;
            seconedCard=null;
        }
    }

    public void playSelectedSound(){
        //mp = MediaPlayer.create(this, R.raw.click2);
        //mp.start();
    }
    public void playCorrectSound(){
        //mp = MediaPlayer.create(this, R.raw.correct);
        //mp.start();
    }
    public void playInCorrectSound(){
        //mp.start();
    }
    public void stopSound(){
        if(mp != null){
            mp.stop();
            mp.release();
            mp = null;
        }
    }
    public void clickSound(){
        stopSound();
        //mp = MediaPlayer.create(this, R.raw.brickogg);
        //mp.start();
    }
    private void winDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Continue ?");
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

                singleChoiceDialog();
            }
        }).setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.cancel();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        })
                .show();
    }

    public void singleChoiceDialog(){
        final CharSequence[] items = {"Hiragana", "Katakana", "Banana", "Vowel", "Pinyin", "Kanji", "Vowel2"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select level");
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                // TODO Auto-generated method stub
                //Toast.makeText(getApplicationContext(), items[item], Toast.LENGTH_SHORT).show();
                switch(item){     // Ngoai item ra thi nen set ca time khi xoay hinh, hard --> xoay nhanh
                    case 0:
                        newGame(6, 8, "hira");
                        break;
                    case 1:
                        newGame(6, 8, "kata");
                        break;
                    case 2:
                        newGame(5, 8, "hira");
                        break;
                    case 3:
                        newGame(6, 8, "vowel");
                    case 4:
                        newGame(6, 9, "pinyin");
                    case 5:
                        newGame(6, 8, "vowel");
                    case 6:
                        newGame(6, 8, "vowel2"); // only 23 vowel so max 46 card (pair is 2 same card)
                }
            }
        }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private List<Drawable> loadHira() {
        images = new ArrayList<Drawable>();

        images.add(getResources().getDrawable(R.drawable.hira_a));
        images.add(getResources().getDrawable(R.drawable.hira_chi));
        images.add(getResources().getDrawable(R.drawable.hira_e));
        images.add(getResources().getDrawable(R.drawable.hira_fu));
        images.add(getResources().getDrawable(R.drawable.hira_ha));
        images.add(getResources().getDrawable(R.drawable.hira_he));
        images.add(getResources().getDrawable(R.drawable.hira_hi));
        images.add(getResources().getDrawable(R.drawable.hira_i));
        images.add(getResources().getDrawable(R.drawable.hira_ka));
        images.add(getResources().getDrawable(R.drawable.hira_ke));
        images.add(getResources().getDrawable(R.drawable.hira_ki));
        images.add(getResources().getDrawable(R.drawable.hira_ko));
        images.add(getResources().getDrawable(R.drawable.hira_ku));
        images.add(getResources().getDrawable(R.drawable.hira_ma));
        images.add(getResources().getDrawable(R.drawable.hira_me));
        images.add(getResources().getDrawable(R.drawable.hira_mi));
        images.add(getResources().getDrawable(R.drawable.hira_mo));
        images.add(getResources().getDrawable(R.drawable.hira_mu));
        images.add(getResources().getDrawable(R.drawable.hira_n));
        images.add(getResources().getDrawable(R.drawable.hira_na));
        images.add(getResources().getDrawable(R.drawable.hira_ne));
        images.add(getResources().getDrawable(R.drawable.hira_ni));
        images.add(getResources().getDrawable(R.drawable.hira_no));
        images.add(getResources().getDrawable(R.drawable.hira_nu));
        images.add(getResources().getDrawable(R.drawable.hira_jo));
        images.add(getResources().getDrawable(R.drawable.hira_o));
        images.add(getResources().getDrawable(R.drawable.hira_ra));
        images.add(getResources().getDrawable(R.drawable.hira_re));
        images.add(getResources().getDrawable(R.drawable.hira_ri));
        images.add(getResources().getDrawable(R.drawable.hira_ro));
        images.add(getResources().getDrawable(R.drawable.hira_ru));
        images.add(getResources().getDrawable(R.drawable.hira_sa));
        images.add(getResources().getDrawable(R.drawable.hira_se));
        images.add(getResources().getDrawable(R.drawable.hira_shi));
        images.add(getResources().getDrawable(R.drawable.hira_so));
        images.add(getResources().getDrawable(R.drawable.hira_su));
        images.add(getResources().getDrawable(R.drawable.hira_tsu));
        images.add(getResources().getDrawable(R.drawable.hira_ta));
        images.add(getResources().getDrawable(R.drawable.hira_te));
        images.add(getResources().getDrawable(R.drawable.hira_to));
        images.add(getResources().getDrawable(R.drawable.hira_tsu));
        images.add(getResources().getDrawable(R.drawable.hira_u));
        images.add(getResources().getDrawable(R.drawable.hira_ya));
        images.add(getResources().getDrawable(R.drawable.hira_yo));
        images.add(getResources().getDrawable(R.drawable.hira_yu));
        images.add(getResources().getDrawable(R.drawable.hira_yo));

        return images;
    }

    private List<Drawable> loadKata() {
        images = new ArrayList<Drawable>();

        images.add(getResources().getDrawable(R.drawable.a));
        images.add(getResources().getDrawable(R.drawable.chi));
        images.add(getResources().getDrawable(R.drawable.e));
        images.add(getResources().getDrawable(R.drawable.fu));
        images.add(getResources().getDrawable(R.drawable.ha));
        images.add(getResources().getDrawable(R.drawable.he));
        images.add(getResources().getDrawable(R.drawable.hi));
        images.add(getResources().getDrawable(R.drawable.i));
        images.add(getResources().getDrawable(R.drawable.ka));
        images.add(getResources().getDrawable(R.drawable.ke));
        images.add(getResources().getDrawable(R.drawable.ki));
        images.add(getResources().getDrawable(R.drawable.ko));
        images.add(getResources().getDrawable(R.drawable.ku));
        images.add(getResources().getDrawable(R.drawable.ma));
        images.add(getResources().getDrawable(R.drawable.me));
        images.add(getResources().getDrawable(R.drawable.mi));
        images.add(getResources().getDrawable(R.drawable.mo));
        images.add(getResources().getDrawable(R.drawable.mu));
        images.add(getResources().getDrawable(R.drawable.n));
        images.add(getResources().getDrawable(R.drawable.na));
        images.add(getResources().getDrawable(R.drawable.ne));
        images.add(getResources().getDrawable(R.drawable.ni));
        images.add(getResources().getDrawable(R.drawable.no));
        images.add(getResources().getDrawable(R.drawable.nu));
        images.add(getResources().getDrawable(R.drawable.o2));
        images.add(getResources().getDrawable(R.drawable.o));
        images.add(getResources().getDrawable(R.drawable.ra));
        images.add(getResources().getDrawable(R.drawable.re));
        images.add(getResources().getDrawable(R.drawable.ri));
        images.add(getResources().getDrawable(R.drawable.ro));
        images.add(getResources().getDrawable(R.drawable.ru));
        images.add(getResources().getDrawable(R.drawable.sa));
        images.add(getResources().getDrawable(R.drawable.se));
        images.add(getResources().getDrawable(R.drawable.shi));
        images.add(getResources().getDrawable(R.drawable.so));
        images.add(getResources().getDrawable(R.drawable.su));
        images.add(getResources().getDrawable(R.drawable.su2));
        images.add(getResources().getDrawable(R.drawable.ta));
        images.add(getResources().getDrawable(R.drawable.te));
        images.add(getResources().getDrawable(R.drawable.to));
        images.add(getResources().getDrawable(R.drawable.tsu));
        images.add(getResources().getDrawable(R.drawable.u));
        images.add(getResources().getDrawable(R.drawable.ya));
        images.add(getResources().getDrawable(R.drawable.yo));
        images.add(getResources().getDrawable(R.drawable.yu));
        images.add(getResources().getDrawable(R.drawable.wo));

        return images;
    }

    private List<Drawable> loadPinyin() {
        images = new ArrayList<Drawable>();

        images.add(getResources().getDrawable(R.drawable.card1));
        images.add(getResources().getDrawable(R.drawable.card2));
        images.add(getResources().getDrawable(R.drawable.card3));
        images.add(getResources().getDrawable(R.drawable.card4));
        images.add(getResources().getDrawable(R.drawable.card5));
        images.add(getResources().getDrawable(R.drawable.card6));
        images.add(getResources().getDrawable(R.drawable.card7));
        images.add(getResources().getDrawable(R.drawable.card8));
        images.add(getResources().getDrawable(R.drawable.card9));
        images.add(getResources().getDrawable(R.drawable.card10));
        images.add(getResources().getDrawable(R.drawable.card11));
        images.add(getResources().getDrawable(R.drawable.card12));
        images.add(getResources().getDrawable(R.drawable.card13));
        images.add(getResources().getDrawable(R.drawable.card14));
        images.add(getResources().getDrawable(R.drawable.card15));
        images.add(getResources().getDrawable(R.drawable.card16));
        images.add(getResources().getDrawable(R.drawable.card17));
        images.add(getResources().getDrawable(R.drawable.card18));
        images.add(getResources().getDrawable(R.drawable.card19));
        images.add(getResources().getDrawable(R.drawable.card20));

        images.add(getResources().getDrawable(R.drawable.bach));
        images.add(getResources().getDrawable(R.drawable.bat));
        images.add(getResources().getDrawable(R.drawable.bi));
        images.add(getResources().getDrawable(R.drawable.hoa));
        images.add(getResources().getDrawable(R.drawable.huyet));
        images.add(getResources().getDrawable(R.drawable.lap));
        images.add(getResources().getDrawable(R.drawable.manh));
        images.add(getResources().getDrawable(R.drawable.mau));
        images.add(getResources().getDrawable(R.drawable.muc));
        images.add(getResources().getDrawable(R.drawable.nach));
        images.add(getResources().getDrawable(R.drawable.nhuu));
        images.add(getResources().getDrawable(R.drawable.that));
        images.add(getResources().getDrawable(R.drawable.thi));
        images.add(getResources().getDrawable(R.drawable.truc));

        return images;
    }

    private List<Drawable> loadVowel() {
        images = new ArrayList<Drawable>();

        images.add(getResources().getDrawable(R.drawable.pya));
        images.add(getResources().getDrawable(R.drawable.bya));
        images.add(getResources().getDrawable(R.drawable.jya));
        images.add(getResources().getDrawable(R.drawable.gya));
        images.add(getResources().getDrawable(R.drawable.rya));
        images.add(getResources().getDrawable(R.drawable.mya));
        images.add(getResources().getDrawable(R.drawable.hya));
        images.add(getResources().getDrawable(R.drawable.nya));
        images.add(getResources().getDrawable(R.drawable.cya));
        images.add(getResources().getDrawable(R.drawable.shya));
        images.add(getResources().getDrawable(R.drawable.kya));
        images.add(getResources().getDrawable(R.drawable.pyu));
        images.add(getResources().getDrawable(R.drawable.byu));
        images.add(getResources().getDrawable(R.drawable.jyu));
        images.add(getResources().getDrawable(R.drawable.gyu));
        images.add(getResources().getDrawable(R.drawable.ryu));
        images.add(getResources().getDrawable(R.drawable.myu));
        images.add(getResources().getDrawable(R.drawable.hyu));
        images.add(getResources().getDrawable(R.drawable.nyu));
        images.add(getResources().getDrawable(R.drawable.cyu));
        images.add(getResources().getDrawable(R.drawable.shyu));
        images.add(getResources().getDrawable(R.drawable.kyu));
        images.add(getResources().getDrawable(R.drawable.pyo));
        images.add(getResources().getDrawable(R.drawable.byo));
        images.add(getResources().getDrawable(R.drawable.jyo));
        images.add(getResources().getDrawable(R.drawable.gyo));
        images.add(getResources().getDrawable(R.drawable.ryo));
        images.add(getResources().getDrawable(R.drawable.myo));
        images.add(getResources().getDrawable(R.drawable.hyo));
        images.add(getResources().getDrawable(R.drawable.nyo));
        images.add(getResources().getDrawable(R.drawable.cyo));

        return images;
    }

    private List<Drawable> loadVowel2() {
        images = new ArrayList<Drawable>();

        images.add(getResources().getDrawable(R.drawable.pa));
        images.add(getResources().getDrawable(R.drawable.ba));
        images.add(getResources().getDrawable(R.drawable.da));
        images.add(getResources().getDrawable(R.drawable.za));
        images.add(getResources().getDrawable(R.drawable.ga));
        images.add(getResources().getDrawable(R.drawable.pi));
        images.add(getResources().getDrawable(R.drawable.bi));
        images.add(getResources().getDrawable(R.drawable.ji));
        images.add(getResources().getDrawable(R.drawable.jzi));
        images.add(getResources().getDrawable(R.drawable.gi));
        images.add(getResources().getDrawable(R.drawable.pu));
        images.add(getResources().getDrawable(R.drawable.bu));
        images.add(getResources().getDrawable(R.drawable.ju));
        images.add(getResources().getDrawable(R.drawable.ju));
        images.add(getResources().getDrawable(R.drawable.gu));
        images.add(getResources().getDrawable(R.drawable.pe));
        images.add(getResources().getDrawable(R.drawable.be));
        images.add(getResources().getDrawable(R.drawable.de));
        images.add(getResources().getDrawable(R.drawable.ze));
        images.add(getResources().getDrawable(R.drawable.ge));
        images.add(getResources().getDrawable(R.drawable.po));
        images.add(getResources().getDrawable(R.drawable.bo));
        images.add(getResources().getDrawable(R.drawable.doo));
        images.add(getResources().getDrawable(R.drawable.zo));

        return images;
    }
}