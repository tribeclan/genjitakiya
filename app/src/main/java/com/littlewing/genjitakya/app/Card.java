package com.littlewing.genjitakya.app;

import android.widget.Button;

/**
 * Created by nickfarrow on 7/23/14.
 */

public class Card {
    public int x;
    public int y;
    public Button button;
    public Card(Button button, int x, int y){
        this.x = x;
        this.y = y;
        this.button = button;
    }
}